import java.util.Scanner;
public class Logic {
	

	private int segmentos;
	private double totalSimpson;
	private double largoSegmento;
	private double valorX;
	private int gLibertad;
	private int testDecidido;
	

	public void logic1a() {
		Scanner input = new Scanner(System.in);
		Output myOut = new Output();
		SimpsonClase myObject = new SimpsonClase();
		//Scan myScan = new Scan();
		System.out.println("Test1: valorX=1.1 & gLibertad=9 \nTest2: valorX=1.1812 & gLibertad=10 \nTest3: valorX=2.750 & gLibertad=30");
		
		testDecidido = input.nextInt();
		switch (testDecidido){
			case 1:
				valorX = 1.1;
				gLibertad = 9;
				break;
			case 2:
				valorX = 1.1812;
				gLibertad = 10;
				break;
			case 3:
				valorX = 2.750;
				gLibertad = 30;
				break;
			default:
				System.out.println("El switch no se está realizando bien.");
		}
		//gLibertad = myScan.reInt("Dame un integer para la variable gLibertad");
		//valorX = myScan.reDouble("Dame un double para la variable valorX");
		
		segmentos = 10;
		
		largoSegmento = (valorX/segmentos);

		totalSimpson = myObject.calcSimpson(segmentos, gLibertad, largoSegmento);

		String outText = ("El valor de la integral fue "+ totalSimpson);
		myOut.writeData("outFile.txt", totalSimpson, outText);
		
		
		
		
		
	}
}
