import java.util.Scanner;

public class Scan {
	/* Creacion de Variables */
	int wishedValueINT = 0;
	double wishedValueDBL = 0;
	
	
	public int reInt(String name){
		Scanner inputINT = new Scanner(System.in);
		System.out.println(name);
		wishedValueINT = inputINT.nextInt();
		return wishedValueINT;
	}
	
	public double reDouble(String name){
		Scanner inputDBL = new Scanner(System.in);
		System.out.println(name);
		wishedValueDBL = inputDBL.nextDouble();
		return wishedValueDBL;
	}
}