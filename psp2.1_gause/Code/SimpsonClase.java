import java.util.Scanner;
public class SimpsonClase{
	
	public double getGamma1(int gLibertad){
		
		double degreeLibertadDBL = gLibertad;
		
		double gamma1;
		
		gamma1 = (degreeLibertadDBL + 1) / 2;
		
		return gamma1;
		
	}
	
	public double getGamma2(int gLibertad){
		
		double degreeLibertadDBL = gLibertad;
		
		double gamma2;
		
		gamma2 = (degreeLibertadDBL) / 2;
		
		return gamma2;
	}
	
	public double getFactorial(int gLibertad){
		
		SimpsonClase objSimpson = new SimpsonClase();
		double gamma1 = objSimpson.getGamma1(gLibertad);
	
		double factorialValor = 1;
	
		for (double i = (gamma1 -1); i>0; i--){
			if (i == .5){
				factorialValor *= i;
				factorialValor *= Math.pow(Math.PI,.5);
			} else {
				factorialValor *= i;
			}
		}
		
		return factorialValor;
	}
	
	public double getRealFactorial(int gLibertad){
		
		SimpsonClase objSimpson = new SimpsonClase();
		double gamma2 = objSimpson.getGamma2(gLibertad);
	
		double factorialReal = 1;
	
		for (double i = (gamma2 -1); i>0; i--){
			if (i == .5){
				factorialReal *= i;
				factorialReal *= Math.pow(Math.PI,.5);
			} else {
				factorialReal *= i;
			}
		}
		
		return factorialReal;
	}
	
	public double[] arregloXi (int segmentos, double largoSegmento){
		
		double[] arregloXi = new double[segmentos+1];
		
		for (int i=0; i<(segmentos+1); i++){
			arregloXi[i] = i * largoSegmento;
		}
		
		return arregloXi;
	}
	
	public int[] arregloMultiplier(int segmentos){
		
		int[] arregloMultiplier = new int[segmentos+1];
		
		arregloMultiplier[0] = 1;
		arregloMultiplier[segmentos] = 1;
		
		for (int i=1; i<segmentos; i++){
			if ( (i%2) == 0) arregloMultiplier[i] = 2;
			else arregloMultiplier[i] = 4;
		}
		
		return arregloMultiplier;
	}
	
	public double[] simp1(int segmentos, int gLibertad, double largoSegmento){
	
	SimpsonClase objSimpson = new SimpsonClase();
	double[] arregloXi = objSimpson.arregloXi(segmentos, largoSegmento);
	double[] simp1 = new double[segmentos+1];
	
	for (int i=0; i<(segmentos+1); i++)
		simp1[i] = 1 + ( Math.pow(arregloXi[i],2) / gLibertad );
	
	return simp1;
	}
	
	public double[] simp2 (int segmentos, int gLibertad, double largoSegmento){
	
	SimpsonClase objSimpson = new SimpsonClase();
	double[] simp1 = objSimpson.simp1(segmentos, gLibertad, largoSegmento);
	double gamma1 = objSimpson.getGamma1(gLibertad);
	double[] simp2 = new double[segmentos+1];
	
	for (int i=0; i<(segmentos+1); i++){
		simp2[i] = Math.pow(simp1[i] , -1 * gamma1);
	}
	
	return simp2;
	}

	public double simpGamma (int segmentos, int gLibertad){
		SimpsonClase  objSimpson = new SimpsonClase();
		double factorialValor = objSimpson.getFactorial(gLibertad);
		double factorialReal = objSimpson.getRealFactorial(gLibertad);
		double simpGamma;
	
		simpGamma = factorialValor / ( Math.sqrt(gLibertad*Math.PI ) * factorialReal);
		
		return simpGamma;
	}
	
	public double[] simpFx (int segmentos, int gLibertad, double largoSegmento){
	
		SimpsonClase objSimpson = new SimpsonClase();
		double[] simp2 = objSimpson.simp2(segmentos, gLibertad, largoSegmento);
		double simpGamma = objSimpson.simpGamma(segmentos, gLibertad);
		double[] simpFx = new double[segmentos+1];
		
		for (int i=0; i<(segmentos+1); i++)
			simpFx[i] = simp2[i] * simpGamma;
		
		return simpFx;
	}
	
	public double[] simpTerms(int segmentos, int gLibertad, double largoSegmento){
	
	SimpsonClase objSimpson = new SimpsonClase();
	double[] simpFx = objSimpson.simpFx(segmentos, gLibertad, largoSegmento);
	int[] arregloMultiplier = objSimpson.arregloMultiplier(segmentos);
	double[] simpTerms = new double[segmentos+1];
	
	for (int i=0; i<(segmentos+1); i++)
		simpTerms[i] = (largoSegmento / 3) * arregloMultiplier[i] * simpFx[i];
	
	return simpTerms;
	}
	
	public double calcSimpson(int segmentos, int gLibertad, double largoSegmento){
	
	SimpsonClase objSimpson = new SimpsonClase();
	double[] simpTerms = objSimpson.simpTerms(segmentos, gLibertad, largoSegmento);
	double totalSimpson = 0;
	
	for (int i=0; i<(segmentos+1); i++)
		totalSimpson += simpTerms[i];
	return totalSimpson;
	}
}
