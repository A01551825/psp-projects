import java.util.Scanner;
public class Logic {
	

	private int segmentos;
	private double simpsonValorEsperado;
	private double largoSegmento;
	private double valorX;
	private int gLibertad;
	private int testDecidido;
	private double errorMax;
	

	public void logic1a() {
		Scanner input = new Scanner(System.in);
		Output myOut = new Output();
		Gause objGause = new Gause();
		System.out.println("Test1: gLibertad=6, simpsonValorEsperado=0.20 \nTest2: gLibertad=15, simpsonValorEsperado=0.45 \nTest3: gLibertad=4, simpsonValorEsperado=0.495");
		System.out.println("Escribe el número de test que quieres (1, 2 o 3)");
		testDecidido = input.nextInt();
		switch (testDecidido){
			case 1:
				simpsonValorEsperado = 0.20;
				gLibertad = 9;
				break;
			case 2:
				simpsonValorEsperado = 0.45;
				gLibertad = 15;
				break;
			case 3:
				simpsonValorEsperado = 0.495;
				gLibertad = 4;
				break;
			default:
				System.out.println("El switch no se está realizando bien.");
		}

		segmentos = 10;
		errorMax = 0.00001;

		valorX = objGause.llamarSimpson(segmentos, gLibertad, valorX,largoSegmento, errorMax, simpsonValorEsperado);

		String outText = ("El valor de la valorX fue "+ valorX);
		myOut.writeData("outFile.txt", valorX, outText);
		
		
		
		
		
	}
}
