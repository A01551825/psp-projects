public class Gause {
	
	
	
	private double integracionPresente;
	private double diferencia;
	private String signoDiferencia;
	private double ajusteD;
	
	public double llamarSimpson(int segmentos, int gLibertad, double valorX, double largoSegmento, double errorMax, double simpsonValorEsperado){
		SimpsonClase objSimpson = new SimpsonClase();
		
		for (int i=0; i<=1000; i++){
			
			if (i == 0){
				valorX = 1;
				ajusteD = 0.5;
				largoSegmento = valorX / segmentos;
			}			
			integracionPresente = objSimpson.calcSimpson(segmentos, gLibertad, largoSegmento);
			diferencia = simpsonValorEsperado - integracionPresente;
			if (i == 0){
				if (diferencia > 0) signoDiferencia = "+";
				else signoDiferencia = "-";
			}
			
			
			if (Math.abs(diferencia) <= errorMax){
				System.out.println("En i intentos "+i);
				break;
			}
			
			if (signoDiferencia == "+" && diferencia < 0){
				signoDiferencia = "-";
				ajusteD = ajusteD / 2;
			} else if (signoDiferencia == "-" && diferencia > 0){
				signoDiferencia = "+";
				ajusteD = ajusteD / 2;
			}
			
			if (integracionPresente < simpsonValorEsperado){
				valorX = valorX + ajusteD;
			} else {
				valorX = valorX - ajusteD;
			}
			
			largoSegmento = valorX / segmentos;
			System.out.println("Cuanto es valorX = "+valorX);
			
		}
		return valorX;
	}
}