public class Relativity{
	
	public double getAvg(double[] list, int intN){
		Media objMedia = new Media();
		double dblAvg = objMedia.getMedia(list, intN);
		return dblAvg;
		
	}
	
    public double getVar(double[] arrRelatives, int intNum){
        Relativity objRelativity = new Relativity();
       
        double dblVar = 0;
        double dblAvg = objRelativity.getAvg(arrRelatives, intNum);
       
        for (int i=0; i<intNum; i++)
            dblVar += Math.pow( (Math.log(arrRelatives[i]) - dblAvg) , 2 );
        dblVar = dblVar / (intNum-1);
		
        return dblVar;
    }
   
    
    public double getDesv(double[] arrRelatives, int intNum){
        Relativity objRelativity = new Relativity();
       
        double dblDesv;
        double dblVar = objRelativity.getVar(arrRelatives, intNum);
        dblDesv = Math.pow(dblVar,.5);
        return dblDesv;
    }
   
    public double[] getRelativity(double[] arrRelatives, int intNum){
        Relativity objRelativity = new Relativity();
       
	    double[] arrLogNum = new double[5];
        double dblAvg = objRelativity.getAvg(arrRelatives, intNum);
        double dblDesv = objRelativity.getDesv(arrRelatives, intNum);
       
        arrLogNum[0] = Math.exp( dblAvg - 2*dblDesv );
        arrLogNum[1] = Math.exp( dblAvg - dblDesv );
        arrLogNum[2] = Math.exp( dblAvg );
        arrLogNum[3] = Math.exp( dblAvg + dblDesv );
        arrLogNum[4] = Math.exp( dblAvg + 2*dblDesv );
       
        return arrLogNum;
    }
 }
	   
