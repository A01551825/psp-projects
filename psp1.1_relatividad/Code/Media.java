public class Media {

	/* Creación de variables. */
	private double media = 0;

	public double getMedia(double[] data, int n) {

	for (int i=0; i<n; i++){
		media += Math.log(data[i]);
	}
	media = media / n;
	
	return media;
	}
}