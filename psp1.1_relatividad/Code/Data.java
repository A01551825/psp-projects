public class Data {
	public double[] saveData(String data) {
		// TODO - implement Data.saveData
		// throw new UnsupportedOperationException();
		String[] arrString = data.split(",");
		double[] arrDouble = new double[arrString.length];
		
		for (int i = 0; i < arrString.length; i++) {
			arrDouble[i]=Double.parseDouble(arrString[i]);
		}
		return arrDouble;
	}
}