public class EstimateCorrelation{
	
	public double sumX(int[] listX, int n){
		
		double SumX = 0;
		
		for (int i=0; i<n; i++)
			
			SumX += listX[i];
			
		return SumX;
	}
	
	public double sumY(int[] listY, int n){
		
		double SumY = 0;
		
		for (int i=0; i<n; i++)
			
			SumY += listY[i];
			
		return SumY;
		
	}
	
	public double sumXX(int[] listX, int n){
		
		double SumXX = 0;
		
		for (int i=0; i<n; i++)
			
			SumXX += Math.pow(listX[i],2);
			
		return SumXX;
		
	}
	
	public double sumYY(int[] listY, int n){
		
		double SumYY = 0;
		
		for (int i=0; i<n; i++)
			
			SumYY += Math.pow(listY[i],2);
			
		return SumYY;
	}
	
	public double sumXY(int[] listX,int[] listY, int n){
		
		double SumXY = 0;
		
		for (int i=0; i<n; i++)
			
			SumXY += (listX[i]*listY[i]);
			
		return SumXY;
		
	}
	
	public double avgX(int[] listX, int n){
		
		double AvgX;
		
		Media objMedia = new Media();
		   AvgX = objMedia.getMedia(listX, n);
			
		return AvgX;
		
	}
	
	public double avgY(int[] listY, int n){
		
		double AvgY;
		
		Media objMedia = new Media();
		   AvgY = objMedia.getMedia(listY, n);
			
		return AvgY;
		
	}
	
	public double getB1(int[] listX, int[] listY, int n){
		
		double dblB1;
        EstimateCorrelation objEstimate = new EstimateCorrelation();
       
        double SumXY = objEstimate.sumXY(listX, listY, n);
        double AvgX = objEstimate.avgX(listX, n);
        double AvgY = objEstimate.avgY(listY, n);
        double SumXX = objEstimate.sumXX(listX, n);
       
        dblB1 = ( SumXY - n*(AvgX * AvgY) );                 
        dblB1 = ( dblB1 / (SumXX - n*Math.pow(AvgX,2)) );
        return dblB1;
		
	}
	
	public double getR( int[] listX, int[] listY, int n){
		
		double dblR;
        EstimateCorrelation objEstimate = new EstimateCorrelation();
       
        double SumXY = objEstimate.sumXY(listX, listY, n);
        double SumX = objEstimate.sumX(listX, n);
        double SumY = objEstimate.sumY(listY, n);
        double SumXX = objEstimate.sumXX(listX, n);
        double SumYY = objEstimate.sumYY(listY, n);
       
        dblR = ( n*(SumXY) - (SumX * SumY) );
        double tempDivision = ( (n*(SumXX) - SumX) * (n*(SumYY) - SumY) );
        dblR = ( dblR / Math.pow(tempDivision,.5) );
        return dblR;
		
	}
	
	public double getB0( int[] listX, int[] listY, int n){
		
		double dblB0;
        EstimateCorrelation objEstimate = new EstimateCorrelation();
       
        double AvgY = objEstimate.avgY(listY, n);
        double dblB1 = objEstimate.getB1(listX, listY, n);
        double AvgX = objEstimate.avgX(listX, n);
       
        dblB0 = ( AvgY - (dblB1 * AvgX) );
        return dblB0;
		
	}
	
	public double getYk( int[] listX, int[] listY, int n){
		
		double dblYk;
        EstimateCorrelation objEstimate = new EstimateCorrelation();
       
        double dblB0 = objEstimate.getB0(listX, listY, n);
        double dblB1 = objEstimate.getB1(listX, listY, n);
        int intXk = 386;
       
        dblYk = ( dblB0 + (dblB1*intXk) );
        return dblYk;
		
	}
}