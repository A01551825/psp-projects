public class Media {

	/* Creación de variables. */
	private double media = 0;

	public double getMedia(int[] data, int n) {

	for (int i=0; i<n; i++)
		media += data[i];
	media = media / n;
	
	return media;
	}
}